<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailBox extends Model
{
	protected $table = 'mails';

    protected $fillable = [
        'to', 'subject', 'body', 'attachment'
    ];
}

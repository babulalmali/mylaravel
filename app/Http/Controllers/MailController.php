<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MailBox;
use Mail;
use DataTables;

class MailController extends Controller
{

    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/mails/index');
    }

    public function anyData()
    {   
        $mails = MailBox::select(['id', 'to', 'body', 'created_at', 'updated_at']);

        return Datatables::of($mails)
            ->addColumn('action', function ($mail) {
                return '<a href="/mails/delete/'.$mail->id.'" class="btn btn-xs btn-primary">Delete</a>';
            })
            ->make(true);
        //return Datatables::of(MailBox::query())->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin/mails/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        //Validating title and body field
        $this->validate($request, [
            'to'=>'required|email',
            'subject'=>'required|max:200',
            'body' =>'required',
            ]);

        $to = $request['to'];
        $subject = $request['subject']; 
        $body = $request['body'];
        $attachment = $request['attachment'];

        $mail = MailBox::create($request->only('to', 'subject', 'body', 'attachment'));
        $attach = $request->file('attachment');
        $attachPath = '';

        if(isset($attach)){
        $imagename = time().'.'.$attach->getClientOriginalExtension();
        $destinationPath = public_path('/attach');
        $attach->move($destinationPath, $imagename);
        $attachPath = $destinationPath.'/'.$imagename;
        }

        
        $message = '';
        Mail::send('emails.mailbox', ['to' => $to, 'body'=>$body, 'subject'=> $subject ], function ($message) use ($attachPath){
            $message->to(env('FROM'), env('FROM_NAME'))->subject('Mail Box');
            if(!empty($attachPath)){
                $message->attach($attachPath);
            }
            
        });

        //Display a successful message upon save
        return redirect()->route('mails')
            ->with('flash_message', 'Mail,
             '. $mail->to.' sent');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mail = MailBox::findOrFail($id);
        $mail->delete();

        return redirect()->route('mails')
            ->with('flash_message',
             'Mail successfully deleted');
    }
}

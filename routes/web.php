<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('home', 'HomeController@index')->name('home');

Route::resource('users', 'UserController');

Route::resource('roles', 'RoleController');

Route::resource('permissions', 'PermissionController');

//Route::resource('posts', 'PostController');



Route::get('login/github', 'GithubLoginController@redirectToProvider')->name('github');
Route::get('login/github/callback', 'GithubLoginController@handleProviderCallback');

Route::get('login/facebook', 'FacebookLoginController@redirectToProvider')->name('facebook');
Route::get('login/facebook/callback', 'FacebookLoginController@handleProviderCallback');

Route::get('login/google', 'GoogleLoginController@redirectToProvider')->name('google');
Route::get('login/google/callback', 'GoogleLoginController@handleProviderCallback');


Route::group(['prefix' => 'posts'], function()
    {
        Route::get('', ['uses' => 'PostController@index'])->name('posts');
        Route::get('data', ['uses' => 'PostController@anyData'])->name('posts.data');
        Route::get('create', ['uses' => 'PostController@create'])->name('posts.create');
        Route::post('store', ['uses' => 'PostController@store'])->name('posts.store');
        Route::put('{id}', ['uses' => 'PostController@update']);
        Route::delete('{id}', ['uses' => 'PostController@delete']);

    });

Route::group(['prefix' => 'mails'], function()
    {
    	Route::get('', ['uses' => 'MailController@index'])->name('mails');
    	Route::get('compose', ['uses' => 'MailController@create'])->name('mails.compose');
        Route::post('store', ['uses' => 'MailController@store'])->name('mails.store');
        Route::get('data', ['uses' => 'MailController@anyData'])->name('mails.data');
        Route::get('delete/{id}', ['uses' => 'MailController@destroy']);
	});





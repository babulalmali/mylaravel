<ul class="sidebar-menu" data-widget="tree">
  <li class="header">MAIN NAVIGATION</li>
  <li class="active treeview">
    <a href="/">
      <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-table"></i> <span>Articles</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{!! route('posts') !!}"><i class="fa fa-circle-o"></i> List Articles</a></li>
      <li><a href="{!! route('posts.create') !!}"><i class="fa fa-circle-o"></i> Add Article </a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-envelope"></i> <span>Mailbox</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{!! route('mails') !!}"><i class="fa fa-circle-o"></i> List Mails</a></li>
      <li><a href="{!! route('mails.compose') !!}"><i class="fa fa-circle-o"></i> Compose </a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-key"></i> <span>Roles</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{!! route('roles.index') !!}"><i class="fa fa-circle-o"></i> List Roles</a></li>
      <li><a href="{!! route('roles.create') !!}"><i class="fa fa-circle-o"></i> Add </a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-key"></i> <span>Permission</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{!! route('permissions.index') !!}"><i class="fa fa-circle-o"></i> List Permissions</a></li>
      <li><a href="{!! route('permissions.create') !!}"><i class="fa fa-circle-o"></i> Add Permission</a></li>
    </ul>
  </li>
  
</ul>
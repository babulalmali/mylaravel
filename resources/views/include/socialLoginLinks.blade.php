<div class="social-auth-links text-center">
	<p>- OR -</p>
	<a href="{{ route('facebook') }}" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
	Facebook</a>
	<a href="{{ route('google') }}" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
	Google+</a>
</div>
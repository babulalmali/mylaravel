@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Mailbox
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Mailbox</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-3">
      <a href="{!! route('mails.compose') !!}" class="btn btn-primary btn-block margin-bottom">Compose</a>
      @include('admin/mails/leftPanel')
    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive mailbox-messages">
            <table class="table table-hover table-striped">
              <table id="post-data" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Email</th>
                        <th>Body</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
            </table>
            <!-- /.table -->
          </div>
          <!-- /.mail-box-messages -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /. box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
{!! Html::script('bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
{!! Html::script('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
<script type="text/javascript">
    $(function() {
        $('#post-data').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('mails.data') !!}',
            columns: [
            { data: 'id', name: 'id' },
            { data: 'to', name: 'to' },
            { data: 'body', name: 'body' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });

</script>
@endsection
@extends('layouts.admin');
@section('content')
<section class="content-header">
  <h1>
    Mailbox
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Mailbox</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-3">
      <a href="{!! route('mails') !!}" class="btn btn-primary btn-block margin-bottom">Back to Inbox</a>
      @include('admin/mails/leftPanel')
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Compose New Message</h3>
          </div>
          <!-- /.box-header -->
          {{ Form::open(array('route' => 'mails.store','enctype' => 'multipart/form-data')) }}
          <div class="box-body">
            <div class="form-group">
              {{ Form::text('to', null, array('class' => 'form-control','placeholder'=>'To:')) }}
            </div>
            <div class="form-group">
              {{ Form::text('subject', null, array('class' => 'form-control','placeholder'=>'Subject:')) }}
            </div>
            <div class="form-group">
              
              {{ Form::textarea('body', null, array('class' => 'form-control','placeholder'=>'Message')) }}
            </div>
            <div class="form-group">
              <div class="btn btn-default btn-file">
                <i class="fa fa-paperclip"></i> Attachment
                <input type="file" name="attachment">
              </div>
              <p class="help-block">Max. 5MB</p>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <div class="pull-right">
              {{ Form::button('<i class="fa fa-pencil"></i> Draft', array('type' => 'submit','class' => 'btn btn-default')) }}
              {{ Form::button('<i class="fa fa-envelope-o"></i> Send', array('type' => 'submit','class' => 'btn btn-primary')) }}
            </div>
          </div>
          <!-- /.box-footer -->
          {{ Form::close() }}
        </div>
        <!-- /. box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  <script>
  $(function () {
    //Add text editor
    $("#compose-textarea").wysihtml5();
  });
</script>
  @endsection
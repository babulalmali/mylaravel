@extends('layouts.admin')

@section('title', '| Add Role')

@section('content')
<section class="content-header">
  <h1>
    Roles
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Roles</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-3">
      <a href="{!! route('roles.index') !!}" class="btn btn-primary btn-block margin-bottom">Back to List</a>
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Role</h3>
          </div>
          <!-- /.box-header -->
          {{ Form::open(array('url' => 'roles')) }}
          <div class="box-body">
            <div class="form-group">
              {{ Form::text('name', null, array('class' => 'form-control','placeholder'=>'Name')) }}
            </div>
            <h5>Assign Permissions</h5>
            <div class="form-group">
            @foreach ($permissions as $permission)
                {{ Form::checkbox('permissions[]',  $permission->id ) }}
                {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>
            @endforeach
            </div>

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <div class="pull-right">
              {{ Form::button('Add', array('type' => 'submit','class' => 'btn btn-primary')) }}
            </div>
          </div>
          <!-- /.box-footer -->
          {{ Form::close() }}
        </div>
        <!-- /. box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>


@endsection
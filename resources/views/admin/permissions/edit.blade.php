@extends('layouts.admin')

@section('title', '| Edit Permission')

@section('content')
<section class="content-header">
  <h1>
    Permission
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Permission </li>
</ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
      <!-- /.col -->
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit {{$permission->name}}</h3>
        </div>
        <!-- /.box-header -->
        {{ Form::model($permission, array('route' => array('permissions.update', $permission->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with permission data --}}
        <div class="box-body">
            <div class="form-group">
              {{ Form::text('name', null, array('class' => 'form-control', 'placeholder'=>'Permission Name')) }}
          </div>
          
      </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <div class="pull-right">
      {{ Form::button('Edit', array('type' => 'submit','class' => 'btn btn-primary')) }}
  </div>
</div>
<!-- /.box-footer -->
{{ Form::close() }}
</div>
<!-- /. box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
@endsection
@extends('layouts.admin')

@section('title', '| Create Permission')

@section('content')
<section class="content-header">
  <h1>
    Permission
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Create Permission</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
      <!-- /.col -->
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Permission</h3>
        </div>
        <!-- /.box-header -->
        {{ Form::open(array('url' => 'permissions')) }}
        <div class="box-body">
            <div class="form-group">
              {{ Form::text('name', null, array('class' => 'form-control', 'placeholder'=>'Permission Name')) }}
          </div>
          
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="pull-right">
          {{ Form::button('Add', array('type' => 'submit','class' => 'btn btn-primary')) }}
      </div>
  </div>
  <!-- /.box-footer -->
  {{ Form::close() }}
</div>
<!-- /. box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->


@endsection
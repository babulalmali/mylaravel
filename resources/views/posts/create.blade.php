@extends('layouts.admin')

@section('title', '| Create New Post')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create New Post
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{!! route('posts') !!}">Posts</a></li>
        <li class="active">Create Post</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <!-- left column -->
        <div class="col-md-12">
        <!-- general form elements disabled -->
        {{ Form::open(array('route' => 'posts.store')) }}
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Create Post</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form">
                <!-- text input -->
                <div class="form-group">
                    {{ Form::label('title', 'Title') }}
                    {{ Form::text('title', null, array('class' => 'form-control','placeholder'=>'Title')) }}
                </div>
                <div class="form-group">
                  {{ Form::label('body', 'Post Body') }}
                  {{ Form::textarea('body', null, array('class' => 'form-control','placeholder'=>'Body')) }}
                </div>
                
              </form>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                {{ Form::submit('Create Post', array('class' => 'btn btn-success')) }}
              </div>
          </div>
          <!-- /.box -->
          {{ Form::close() }}
        </div>
    </div>
    </section>
@endsection